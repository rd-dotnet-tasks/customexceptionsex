﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptionsEx
{
    class InvalidNameException : Exception
    {
        public InvalidNameException()
        {

        }

        public InvalidNameException(string name)
            : base(String.Format("Invalid Name: {0}", name))
        {

        }
    }

    class InvalidAgeException : Exception
    {
        public InvalidAgeException()
        {

        }

        public InvalidAgeException(int age)
            : base(String.Format("Invalid Age: {0}", age))
        {

        }
    }
}
