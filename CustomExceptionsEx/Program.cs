﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CustomExceptionsEx
{
    class Program
    {
        private static void ValidateName(string firstName)
        {
            Regex regex = new Regex("^[a-zA-Z]+$");

            if (!regex.IsMatch(firstName))
                throw new InvalidNameException(firstName);
        }

        private static void ValidateAge(int age)
        {
            if (age < 0 || age > 130)
                throw new InvalidAgeException(age);
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Enter First Name");
                string firstName = Console.ReadLine();
                ValidateName(firstName);
                Console.WriteLine("Enter Age");
                Int32.TryParse(Console.ReadLine(), out int age);
                ValidateAge(age);
            }
            catch (InvalidNameException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }
}
